"use strict";

XPCOMUtils.defineLazyGetter(this, "etld", () =>
    Cc["@mozilla.org/network/effective-tld-service;1"]
	.getService(Ci.nsIEffectiveTLDService)
);

XPCOMUtils.defineLazyGetter(this, "domainIsolator", () =>
    Cc["@torproject.org/domain-isolator;1"]
	.getService(Ci.nsISupports).wrappedJSObject
);

const domainPattern = /^[a-zA-Z]([a-zA-Z-\d]*[a-zA-Z\d])?([.][a-zA-Z]([a-zA-Z-\d]*[a-zA-Z\d])?)*$/;

this.torbrowser = class extends ExtensionAPI {
    getAPI(context) {
	return {
	    torbrowser: {
		newCircuitForDomain: function(domain) {
		    return new Promise((resolve, reject) => {
			if (typeof domain !== 'string') {
			    return reject("typeof domain is not 'string'");
			}

			if (!domainPattern.test(domain)) {
			    return reject('"${domain}" is not a valid domain name');
			}

			let firstPartyDomain = etld.getBaseDomainFromHost(domain);
			domainIsolator.newCircuitForDomain(firstPartyDomain);
			resolve();
		    });
		},
	    },
	};
    }
};
