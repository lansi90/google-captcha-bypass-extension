#!/bin/sh
set -e

if [ $# -lt 1 ]; then
  echo "usage: $0 omni.ja"
  echo
  echo patch omni.ja with the torbrowser browser extension api.
  exit 1
fi

errors=0
for prog in zip unzip awk ; do
  if ! command -v $prog >/dev/null; then
    echo "'$prog' not in \$PATH"
    errors=1
  fi
done
[ $errors -ne 0 ] && exit 2

omnija="$1"
if [ ! -e "$omnija" ]; then
  echo "file '$omnija' does not exist."
  exit 3
fi

rootdir=`realpath $(dirname $0)`
workdir="$rootdir"/.work

alias pushd="command pushd >/dev/null"
alias popd="command popd >/dev/null"

[ -d "$workdir" ] && rm -r "$workdir"
mkdir "$workdir"
pushd "$workdir"

merge_json_files() {
  command awk -v b="$(<$2)" '
    BEGIN { RS="\0"; LF="[\n\r]*"; SP="[ \t\n\r]*";
            LB="^" SP "{" LF; RB=LF "}" SP "$";
            RBB="}" SP "}" SP "$"}
    { sub(LB, "", b); sub(RB, "", b);
      i = match($0, RBB); if (!i) { exit 1 }
      print substr($0, 1, i) ",\n" b substr($0, i+1) }
  ' "$1"
}

backup_file() {
  local n=0 file="$1"
  [ -e "$file" ] || exit 1
  while [ -e "$file".$n.bak ]; do n=$[++n]; done
  cp "$file" "$file".$n.bak
}

backup_file "$omnija"
extensions_path=chrome/toolkit/content/extensions
mkdir -p $extensions_path
command unzip -qq "$omnija" $extensions_path/ext-toolkit.json
pushd $extensions_path
if ! grep -q torbrowser ext-toolkit.json; then
  merge_json_files ext-toolkit.json "$rootdir"/ext-toolkit.json >ext-toolkit.json.tmp
  mv ext-toolkit.json.tmp ext-toolkit.json
fi
cp -r "$rootdir"/parent .
cp -r "$rootdir"/schemas .
popd

command zip -9qr "$omnija" .
popd
rm -r "$workdir"

echo "'$omnija' was successfully patched with the torbrowser browser extension api."
