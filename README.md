# Google captcha page skipper browser extension for TorBrowser

## Table of Contents

- [Google captcha page skipper browser extension for TorBrowser](#google-captcha-page-skipper-browser-extension-for-torbrowser)
    - [Introduction](#introduction)
    - [Installation](#installation)
        - [Exposing the "New circuit" functionality to the browser extension API](#exposing-the-new-circuit-functionality-to-the-browser-extension-api)
        - [Install the signed browser extension](#install-the-signed-browser-extension)
    - [Usage](#usage)
    - [See Also](#see-also)

## Introduction

Browsing Google and YouTube anonymously through TorBrowser can be
cumbersome, when google casually redirects their captcha page
https://www.google.com/sorry/index, which often is blank and contains
no captcha. The manual workaround to this problem is to keep changing
the tor circuit by clicking the button: "New tor circuit for this
site".

This project is a browser extension that automates the process of trying new
tor circuits until the blocking captcha page goes away.

## Installation

### Exposing the "New circuit" functionality to the browser extension API

Before installing the browser extension, you need to patch the file
"omni.ja" to expose the "New tor circuit" functionality to the browser
extension API. First you need to locate the file `omni.ja`, you should
find it in the installation directory of TorBrowser. Once you find it,
run the shell script `torbrowser-extension-api/patch-omnija.sh` to
patch it.

For example:

```shell
$ cd torbrowser-extension-api
$ ./patch-omnija.sh ~/torbrowser/Browser/omni.ja
```

Note that you need to run this script to patch `omni.ja` each time
TorBrowser receives an update.

### Install the signed browser extension

The browser extension was signed at https://addons.mozilla.org.
But, since it uses an extension API that is not part of standard
Firefox, it doesn't meet the criteria for publishing on the website.

To manually install the signed extension file
`captcha-bypass-extension/extension-signed.xpi`, in TorBrowser, visit
the "Add-ons" page, and click "Install Add-on From File..." under the
button with a gear icon.

## Usage

The browse extension works automatically without user interaction. To
watch it in action, visit the page
[https://www.google.com/sorry/index?continue=https://www.youtube.com](https://www.google.com/sorry/index?continue=https://www.youtube.com/);
If the web-extension is working; youtube.com loads directly.

You can disable or remove the extension from the add-ons page.

## See Also
[AutoCookieOptout](https://github.com/CodyMcCodington/AutoCookieOptout)
is a browser extension that automates clicking through cookie consent
dialogs. It works for a list of websites that includes Google and
YouTube.
