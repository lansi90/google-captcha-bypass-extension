async function listener(details) {
  let url = new URL(details.url);
  let targetURL = new URL(url.searchParams.get('continue'));
  await browser.torbrowser.newCircuitForDomain(targetURL.hostname);
  return {redirectUrl: targetURL.href};
}

browser.webRequest.onBeforeRequest.addListener(
  listener,
  {urls: ["*://www.google.com/sorry/index?*continue=*"]},
  ['blocking']
)
